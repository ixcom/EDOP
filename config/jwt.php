<?php

return [
	'iat'	=> time(),
	'nbf'	=> time(),							// 获取0s后可用
	'exp'	=> 3600 * 48 + time(),				// 过期时间
	'iss'	=> 'http://www.ixcom.cn',			// 签发者，可选
	'aud'	=> 'ixcom',							// 接收该JWT的一方，可选
	'key'	=> "!@#$%^&*~"						// 私钥
];