<?php

use think\facade\Route;

/* 公共上传接口 */
Route::get('upload/index', '\app\base\controller\Upload@index');
Route::post('upload/file', '\app\base\controller\Upload@file');
Route::post('upload/image', '\app\base\controller\Upload@image');

Route::post('upload/avatar', '\app\base\controller\Upload@avatar');

// 当路由规则都不匹配的话，就会跳到`miss`
Route::miss(function () {
    return json(['code'=>10001, 'msg'=>"异常访问"]);
});