<?php
namespace app\common\validate;

use think\Validate;

class DataUser extends Validate
{
	// 验证规则
	protected $rule = [
		'mobile' => 'require|mobile|unique:DataUser',
		'realname' => 'require|chs',
	];

	// 错误消息
	protected $message = [
		'mobile.require' => '手机号不能为空~',
		'mobile.mobile' => '该手机号码不正确',
		'mobile.unique' => '该手机号码已被使用',
		'realname.require' => '姓名不能为空~',
		'realname.chs'	=> '姓名必须汉字~',
	];

	// 验证场景
	protected $scene = [
		'bind' => ['mobile'],
		'complete' => ['mobile', 'realname']
	];

	// 自定义验证规则
}