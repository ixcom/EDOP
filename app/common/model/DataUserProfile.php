<?php
namespace app\common\model;

use think\Model;

class DataUserProfile extends Model
{
	protected $table = 'data_user_profile';
	// 设置主键字段
	protected $pk = 'user_id';
	// 设置字段缓存
	// protected $schema = [
	// 	'id'	=> 'int',
	// 	'realname'		=> 'string',
	// 	'amount'		=> 'int',
	// 	'remark'		=> 'str',
	// 	'status'		=> 'int',
	// 	'create_time'	=> 'int',
	// ];
	public function user()
	{
		return $this->belongsTo(DataUser::class, 'user_id', 'id');
	}
}