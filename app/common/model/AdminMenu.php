<?php

namespace app\common\model;

use think\Model;

class AdminMenu extends Model
{
	protected $table = "admin_menu";

	// 搜索器
	public function searchKeywordAttr ($query, $value, $array)
	{
		if (!empty($value)) {
			$query->where("title|icon|path", 'like', '%' . $value . '%');
		}
	}
}