<?php
namespace app\common\model;

use think\Model;

class SystemMenu extends Model
{
	protected $table = "system_menu";

	/**
	 * 模型事件 -> 新增数据后
	 */
	// public static function onAfterInsert($m)
	// {
	// 	$menu = self::find($m->id);
	// 	$meta = [
	// 		'title' => request()->param('meta.title'),
	// 		'icon' => request()->param('meta.icon'),
	// 		'type' => request()->param('meta.type'),
	// 		'affix' => request()->param('meta.affix'),
	// 	];
	// 	$menu->meta()->save($meta);
	// }

	// /**
	//  * 模型事件 -> 数据更新后
	//  */ 
	// public static function onAfterUpdate ($m) {
	// 	$menu = self::find($m->id);
	// 	$meta = [
	// 		'title' => request()->param('meta.title'),
	// 		'icon' => request()->param('meta.icon'),
	// 		'type' => request()->param('meta.type'),
	// 		'affix' => request()->param('meta.affix'),
	// 		'color' => request()->param('meta.color'),
	// 		'hidden' => request()->param('meta.hidden'),
	// 		'active' => request()->param('meta.active'),
	// 	];
	// 	// $menu->meta
	// 	$menu->meta->save($meta);
	// }

	// // $meta['type'] = request()->post('meta.title');


	// /* 递归算法 */
	// static public $treeMenu = array();
	// static public function treeList ($data, $pid = 0)
	// {
	// 	foreach ($data as $k => $v) {
	// 		if ($v['parentId'] == $pid) {
	// 			$child = self::getChildMenu($data, $v['id']);
	// 			if (!empty($child)) {
	// 				$v['children'] = $child;
	// 			}
	// 			self::$treeMenu[] = $v;
	// 		}
	// 	}
	// 	return self::$treeMenu;
	// }

	// static public function getChildMenu ($data, $pid) {
	// 	$arr = [];
	// 	foreach ($data as $v) {
	// 		if ($v['parentId'] == $pid) {
	// 			$arr[] = $v;
	// 		}
	// 	}
	// 	return $arr;
	// }


	// /* 一对一关联 */
	// public function meta ()
	// {
	// 	return $this->hasOne(SystemMenuMeta::class, 'menu_id', 'id');
	// }
}