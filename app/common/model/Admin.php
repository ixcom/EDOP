<?php

namespace app\common\model;

use think\Model;

class Admin extends Model
{
	protected $table="admin";

	// 关联模型 - 管理员分组 - role


	/* 清除登录状态 */
	public static function clearLoginStatus ()
	{
		session('admin', null);
		// cookie('admin_token', null);
		// cookie('admin_last_url', null);
	}

	
}