<?php

namespace app\common\model;

use think\Model;

class User extends Model
{
	protected $table = "user";

	public function group ()
	{
		return $this->belongsTo(UserGroup::class, 'group_id')->bind(['group_name' => 'name']);
	}
}