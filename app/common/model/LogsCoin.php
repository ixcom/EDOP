<?php
namespace app\model;

use think\Model;

class User extends Model
{
	protected $table = 'logs_coin';
	// 设置主键字段
	protected $pk = 'log_coin_id';
	// 设置字段缓存
	protected $schema = [
		'log_coin_id'	=> 'int',
		'user_id'		=> 'int',
		'amount'		=> 'int',
		'remark'		=> 'str',
		'status'		=> 'int',
		'create_time'	=> 'int',
	];

}