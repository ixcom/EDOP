<?php

namespace app\common\lib;

use DomainException;
use Firebase\JWT\JWT;
use think\facade\Config;

class Token
{
	public static function createJwt ($admin)
	{
		$jwt = Config::get('jwt');
		// 删除数组中的最后一个
		// $key = array_pop($jwt);
		$key = config('jwt.key');
		$jwt['data'] = $admin;
		return JWT::encode($jwt, $key);
	}

	public static function checkJwt ($token)
	{
		$key = config('jwt.key');
		try {
			JWT::$leeway = 60;
			$decoded = JWT::decode($token, $key, array('HS256'));
			$array = (array)$decoded;

			dump($array);
			return $array;
		} catch (Exception $e) {
			return "";
		}

		// $status = array('code'=>2);
		// if (!$token) {
		// 	$status['msg'] = "token not found";
		// 	return $status;
		// }
		// try {
		// 	JWT::$leeway = 60;
		// 	$decoded = JWT::decode($token, $key, array('HS256'));
		// 	$arr = (array)$decoded;
		// 	// $result['code'] = 1;
		// 	// $result['msg'] = "success";
		// 	// $result['data'] = $arr['data']->user_id;
		// 	// // return $arr['data']->user_id;
		// 	return $arr;
		// } catch (\Firebase\JWT\SignatureInvalidException $e) {
		// 	$status['msg'] = "签名不正确";
		// 	return $status;
		// } catch (\Firebase\JWT\BeforeValidException $e) {
		// 	$status['msg'] = "token 未生效";
		// 	return $status;
		// } catch (\Firebase\JWT\ExpiredException $e) {
		// 	$status['msg'] = "token 过期";
		// 	return $status;
		// } catch (Exception $e) {
		// 	$status['msg'] = "其他错误";
		// 	return $status;
		// }
		
	}


	public static function create() {
		// 生成 token
		return sha1(md5(uniqid(md5(microtime(true)),true)));
	}

	public static function check($token) {
		// 生成 token
		// $token = sha1(md5(uniqid(md5(microtime(true)),true)));
	}

}