<?php
namespace app\common\exception;

class APIException extends BaseException
{
	public $code = 400;
	public $msg = 'apis is error';
	public $errorCode = 10700;

	public $paramsEnum = [
		1 => ['error_code'=>20001, 'msg'=>'手机号已经存在~'],
        2 => ['error_code'=>10702, 'msg'=>'活动添加失败，请重试'],
        3 => ['error_code'=>10703, 'msg'=>'票据规格添加，请重试'],
        4 => ['error_code'=>10704, 'msg'=>'活动不存在'],
        5 => ['error_code'=>10705, 'msg'=>'不可跨账号操作~'],
        6 => ['error_code'=>10706, 'msg'=>'活动不存在或已下架~'],
        7 => ['error_code'=>10707, 'msg'=>'票据不存在~'],
        8 => ['error_code'=>10708, 'msg'=>'该票据已售罄，请更换其他规格~'],
        9 => ['error_code'=>10709, 'msg'=>'活动名额已满，亲下次早来哦~'],
        10 => ['error_code'=>10710, 'msg'=>'下单失败，请重试~']
	];

	public function __construct($params = [], $type = 1)
	{
		parent::__construct($params, $type);
	}
}