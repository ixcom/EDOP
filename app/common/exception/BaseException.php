<?php
namespace app\common\exception;

use think\Exception;
use Throwable;

class BaseException extends Exception
{
	public $code = 400;
	public $msg = 'invalid parameters';
	public $errorCode = 999;
	public $paramsEnum = [];
	public $shouldToClient = true;

	public function __construct($params = [], $type = 0)
	{
		if(!is_array($params)){
			return;
		}
		if (array_key_exists('code', $params)) {
			$this->code = $params['code'];
		}
		if (array_key_exists('msg', $params)) {
			$this->msg = $params['msg'];
		}
		if (array_key_exists('error_code', $params)) {
			$this->errorCode = $params['error_code'];
		}
		if ($type) {
			$params = $this->paramsEnum[$type];
			$this->errorCode = $params['error_code'];
			$this->msg = $params['msg'];
		}
	}
}