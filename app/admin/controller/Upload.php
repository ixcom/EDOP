<?php
namespace app\admin\controller;

use think\facade\Request;

class Upload
{
	public function index ()
	{
		return "index";
	}

	public function file ()
	{
		return "file";
	}

	/**
	 *
	 * @return $code: 200
	 * @return $data: { "id": "", "src": "", "filename": ""}
	 * @return $message
	 */
	public function avatar ()
	{
		$file = Request::file('file');

		$name = \think\facade\Filesystem::disk('public')->putFile('avatar', $file);
		// $name = "avatar/20211029/281c2e6b6f50e77d0e9232b7369ea10b.png";


		$data['id'] = "001";
		$data['src'] = "http://api.ixcom.cn/storage/" . $name;
		$narr = explode("/", $name);
		$data['fileName'] = $narr[count($narr)-1];

		return json(['code' => 200, 'data' => $data, 'message' => "success"]);
	}
}

/*

{
    "code": 200,
    "data": {
        "id": "D8AA1aCD-4cdB-DaA6-5A6E-F3DAc1EFfEc7",
        "src": "http://dummyimage.com/200x200/ae79f2/FFF&text=SCUI&_=.png",
        "fileName": "ffedCFD7-f8C4-dAcA-ebAB-35fe65C64f52.png"
    },
    "message": ""
}



 */