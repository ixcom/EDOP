<?php
namespace app\admin\controller;

use think\facade\View;
use think\facade\Session;

class Index extends AdminController
{
	public function index()
	{
		$list = \think\facade\Db::name('system_menu')->order('sort asc')->select()->toArray();
		$menu = list_to_tree($list);
        View::assign([
            'menu' => $menu
        ]);
		View::assign('current_nav', '1');
		return View::fetch('index/index');
	}

	public function main ()
	{
		return View::fetch('index/main');
	}
}