<?php

namespace app\admin\controller;

use app\common\model\User as UserModel;

use think\facade\View;

class User
{

	public function index()
	{
		// $userList = DataUserModel::select();
		// $userList = UserModel::paginate(20);
		// dump($userList);
		// $page = $userList->render();
		// View::assign('current_nav', "user");
		// View::assign('userList', $userList);
		// View::assign('page', $page);
		return View::fetch('user/index');
	}

	public function lists()
	{
		$param = request()->param();
		$where = array();
		if (!empty($param['keywords'])) {
			$where[] = ['realname|mobile', 'like', '%' . $param['keywords'] . '%'];
		}
		// 按时间查询
		$start_time = isset($param['start_time']) ? strtotime(urldecode($param['start_time'])) : 0;
		$end_time = isset($param['end_time']) ? strtotime(urldecode($param['end_time'])) : 0;
		if ($start_time > 0 && $end_time > 0) {
			if ($start_time === $end_time) {
				$where['create_time'] = array('eq', $start_time);
			} else {
				$where['create_time'] = array(array('egt', $start_time), array('elt', $end_time), 'and');
			}
		} elseif ($start_time > 0 && $end_time == 0) {
			$where['create_time'] = array('egt', $start_time);
		} elseif ($start_time == 0 && $end_time > 0) {
			$where['create_time'] = array('elt', $end_time);
		}
		$rows = empty($param['limit']) ? 10 : $param['limit'];
		// $list = UserModel::where($where)->order('id desc')->paginate($rows,false,['query'=>$param])->each(function($item,$key){

		// });
		$list = UserModel::with('group')->where($where)->order('id asc')->paginate($rows,false,['query'=>$param]);
		return table_data(0, '', $list);
	}


	/* 用户新增 */

	public function create()
	{
		return "add user";
	}
	/* id name password userName  group*/


	/* 用户编辑 */
	public function edit ()
	{

		dump(request()->param('id'));
		return View::fetch('user/edit');
	}
}