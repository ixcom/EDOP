<?php

namespace app\admin\controller;

use app\common\model\SystemRole as SystemRoleModel;

class Role
{
	public function index()
	{
		return "";
	}

	public function lists()
	{
		$role = SystemRoleModel::select();
		return json(['code' => 200, 'data' => $role, 'message' => ""]);
	}
}