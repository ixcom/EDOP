<?php
namespace app\admin\controller;

use app\common\model\SystemMenu;

class System
{
	public function index ()
	{
		return "";
	}



	/* 获取用户菜单 */
	public function menu ()
	{
		return "menu";
	}


  /* 获取版本 */
  public function ver ()
  {
    return success(200, "", "1.3.1");
  }

}


/*


{
  "code": 200,
  "data": {
    "menu": [
      {
        "name": "vab",
        "path": "/vab",
        "meta": {
          "title": "组件",
          "icon": "el-icon-takeaway-box",
          "type": "menu"
        },
        "children": [
          {
            "path": "/vab/drag",
            "name": "drag",
            "meta": {
              "title": "拖拽排序",
              "icon": "el-icon-rank",
              "type": "menu"
            },
            "component": "vab/drag"
          },


 */