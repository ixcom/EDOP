<?php
namespace app\admin\controller;

use think\facade\View;

class Document extends AdminController
{
	public function index ()
	{
		$breadcrumb = array();
		View::assign('current_page', "文章列表");
		View::assign('breadcrumb', $breadcrumb);
		return View::fetch("document/index");
	}

	public function add ()
	{
		View::assign('current_page', "新增文章");
		return View::fetch('document/document_add');
	}

	public function edit ()
	{
		return View::fetch('document/document_edit');
	}
}