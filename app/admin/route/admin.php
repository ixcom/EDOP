<?php
// +----------------------------------------------------------------------
// | Ixyuan
// +----------------------------------------------------------------------

use think\facade\Route;

Route::get('/', '\app\admin\controller\Index@index');
Route::get('/main$', '\app\admin\controller\Index@main');

// 管理员登录入口
Route::get('login$', '\app\admin\controller\Login@index');
Route::post('login/auth', '\app\admin\controller\Login@auth');

// Route::group(function(){
// 	Route::post('system/menu', '\app\admin\controller\System@menu');
// })->middleware('check_token');
Route::get('system/menu$', '\app\admin\controller\Menu@index');
Route::get('system/menu/list$', '\app\admin\controller\Menu@lists');
Route::post('system/menu/add', '\app\admin\controller\Menu@add');
Route::post('system/menu/edit', '\app\admin\controller\Menu@edit');

/* 用户: user */
Route::get('user/index$', '\app\admin\controller\User@index');
Route::get('user/edit$', '\app\admin\controller\User@edit');

/* 会员分组 */
Route::get('group/index$', '\app\admin\controller\Group@index');
Route::get('group/list$', '\app\admin\controller\Group@list');

Route::get('user/list$', '\app\admin\controller\User@lists');
Route::get('role/list$', '\app\admin\controller\Role@lists');
Route::post('user/add$', '\app\admin\controller\User@create');

// Route::get('new/<id>','News/read'); // 定义GET请求路由规则
// Route::post('new/<id>','News/update'); // 定义POST请求路由规则
// Route::put('new/:id','News/update'); // 定义PUT请求路由规则
// Route::delete('new/:id','News/delete'); // 定义DELETE请求路由规则
// Route::any('new/:id','News/read'); // 所有请求都支持的路由规则

/* 上传: upload */
Route::post('upload/avatar$', '\app\admin\controller\Upload@avatar');



/* 文章管理 */
Route::get('document/index$', '\app\admin\controller\Document@index');
Route::get('document/add$', '\app\admin\controller\Document@add');
Route::get('document/edit$', '\app\admin\controller\Document@edit');



/* 系统管理 */
Route::get('system/ver$', '\app\admin\controller\System@ver');

// 当路由规则都不匹配的话，就会跳到`miss`
Route::miss(function () {
    return json(['code'=>10001, 'msg'=>"异常访问"]);
});