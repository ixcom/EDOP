<?php
declare (strict_types = 1);

namespace app\admin\middleware;

use app\common\model\AdminMenu;

/**
 * 菜单检查
 */
class MenuCheck
{
	// 不需要检查的类
	protected $ignoreCheckClass = [
		'login'
	];

	public function handle ($request, \Closure $next)
	{
		if (!in_array($request->class, $this->ignoreCheckClass)) {
			$request->systemMenu = $this->menu($request);

		}
		return $next($request);
	}

	// 系统菜单

	public function menu($request)
	{
		$where[] = ['status', '=', 1];
		// if ($request->userInfo['group_role'] !== '*') {
		// 	$where[] = ['id', 'in', $request->userInfo['group_role']];
		// }
		$menu = AdminMenu::where($where)->select();
		$menu = $menu ? $menu->toArray() : [];
		foreach ($menu as $key => $value) {
			$menu[$key]['unread'] = 0;
		}
		return $menu;
	}
}