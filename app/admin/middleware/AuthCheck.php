<?php
declare(strict_types = 1);

namespace app\admin\middleware;

class AuthCheck
{
	// 不需要检查的类
	protected $ignoreCheckClass = [
		'login'
	];

	public function handle($request, \Closure $next)
	{
		if (!in_array($request->class, $this->ignoreCheckClass)) {
			$request->authorityPathList = array_column($request->menu, 'path');
			$request->authorityIndex = array_search($request->authorityPath, $request->authorityPathList);
			if ($request->path !== 'index/index') {
				if ($request->authorityIndex === false) {
					return $request->isPost() ? json(['status' => 'error', 'message' => '当前权限不足']) : abort(403, '当前权限不足~');
				}
			}
		}
		return $next($request);
	}
}