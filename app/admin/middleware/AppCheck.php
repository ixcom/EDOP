<?php
namespace app\admin\middleware;

class AppCheck
{
	public function handle ($request, \Closure $next)
	{
		// 系统信息
		$pathinfo = str_replace('.html', '', $request->pathinfo());
		$request->path = empty($pathinfo) ? 'index/index' : $pathinfo;
		$request->class = explode('/', $request->path)[0];
		$request->authorityPath = $request->path;
		// 插件信息
		// ...

		return $next($request);
	}
}