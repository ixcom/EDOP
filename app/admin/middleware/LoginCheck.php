<?php
declare(strict_types=1);

namespace app\admin\middleware;
use think\facade\Session;
use think\facade\Cache;

/** 登录检验 **/
class LoginCheck
{
	protected $ignoreCheckClass = ["login"];

	public function handle ($request, \Closure $next)
	{
		if (!in_array($request->class, $this->ignoreCheckClass)) {
			// 验证用户登录状态
			if (!Session::has("sys_admin")) {
				if ($request->isAjax()) {
					return success(404, '请先登录');
				} else {

					// redirect((string) url('admin/index/index'));
					redirect('/admin/login.html')->send();
					exit;
				}
			}
		}
		else {
			$admin = Session::get("sys_admin");
			$request->admin = $admin;
		}
		return $next($request);
	}
}