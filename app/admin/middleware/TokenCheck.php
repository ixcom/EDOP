<?php
namespace app\admin\middleware;

use app\common\lib\Token;

class TokenCheck
{
	protected $ignoreCheckClass = [
		'login'
	];

	public function handle($request, \Closure $next)
	{
		if (!in_array($request->class, $this->ignoreCheckClass)) {
			$token = request()->header('Authorization');
			// 传入 token 为空
			if (!$token) {
				return fail(200, "token is null", 401);
				// http_response_code(401);
				// exit(json_decode(fail(200, '重新登录')));
			}
			// 获取 token 失败
			$data = cache($token);
			if (!$data) {
				return fail(200, "token is fail", 401);
				// http_response_code(401);
				// exit(json_decode(fail(200, '重新登录')));
			}
			$request->admin = $data;
		}
		return $next($request);
	}
}