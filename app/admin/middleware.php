<?php
// +-------------------
// | admin 中间件定义
// +-------------------

return [
	// session 初始化
	// \think\middleware\SessionInit::class,

	// 环境检测
	app\admin\middleware\AppCheck::class,

	// 登录验证
	app\admin\middleware\LoginCheck::class,

	// token验证
	// app\admin\middleware\TokenCheck::class,
	
	// 权限验证
	// app\admin\middleware\AuthCheck::class,
];