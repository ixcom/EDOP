<?php
namespace app\user\controller;

use app\common\model\DataUser as DataUserModel;
use app\common\model\DataUserWxapp as DataUserWxappModel;
use app\common\validate\DataUser as DataUserValidate;
use think\facade\Request;

use app\common\lib\Token;

class Wxapp
{
	protected $appid = "wx864dd1d0ceff3dc7";
	protected $secret = "89b739c64287d0ee105656425d99c291";
	protected $grant_type = "authorization_code";

	public function index()
	{
		return;
	}

	/********************
	 * 微信授权快速登录注册
	 * route: /wxapp/speedy/login
	 ********************/
	public function speedyLogin()
	{	
		$code = Request::param("code");
		$openid = DataUserWxappModel::getWxUserOpenid($code);
		// 根据 openid 获取用户数据
		$user = DataUserModel::hasWhere('wxapp', ['openid'=>$openid])->find();
		if ($user) {
			$r_user = DataUserModel::withJoin('wxapp')->find($user->id);
			// 生成 token
			$r_user['token'] = Token::createJwt($r_user['id']);
			// 删除 password
			unset($r_user['password']);
			return success(200, "获取成功", $r_user);
		} else {
			// 开始注册
			$wxuserinfo = Request::param("userinfo");
			$wxuserinfo['openid'] = $openid;
			$user = new DataUserModel;
			$user->save(['realname' => $wxuserinfo['nickName']]);
			$user->wxapp()->save($wxuserinfo);
			if ($user) {
				// $_user = DataUserModel::hasWhere('wxapp', ['openid'=>$openid])->find();
				$r_user = DataUserModel::withJoin('wxapp')->find($user->id);
				return success(200, "创建成功~", $r_user);
			}
		}
	}


	/*
	 * 通用保存字段
	 */
	public function edit ()
	{
		$id = Request::param("id");
		$field = Request::param("field");
		$value = Request::param("value");

		$data['id'] = $id;
		$data[$field] = $value;
		$user = DataUserModel::update($data);

		if ($user) {
			$r_user = DataUserModel::withJoin('wxapp')->find($id);
			return success(200, "操作成功~", $r_user);
		}

	}

	/**
	 * 小程序完善注册
	 */
	public function complete () {
		$param = Request::only(['realname', 'mobile', 'ext_company']);
		$param['id'] = request()->id;
		try {
			validate(DataUserValidate::class)->scene('complete')->check([
				'realname' => $param['realname'],
				'mobile' => $param['mobile']
			]);
		} catch (ValidateException $e) {
			abort(1001, $e->getMessage());
		}

		$param['complete'] = 1;

		$result = DataUserModel::update($param);

		if ($result) {
			$user = DataUserModel::withJoin('wxapp')->find($result->id);
			return success(200, "操作成功~", $user);
		} else {
			return error(222, "更新失败~");
		}


	}

	public function login()
	{
		$code = Request::param("code");
		$openid = DataUserWxappModel::getWxUserOpenid($code);
		// 根据 openid 获取用户数据
		$user = DataUserModel::hasWhere('wxapp', ['openid'=>$openid])->find();
		if ($user) {
			return success(200, "获取成功", $user);
		} else {
			return success(201, "获取失败", $openid);
		}
	}

	public function register()
	{

	}






	/**
	public function reg()
	{
		// 获取code
		$code = Request::param("code");
		// 获取请求参数
		$post_data = Request::only(['mobile', 'realname']);
		// 获取userinfo
		$wxuserinfo = Request::param("userinfo");
		// 根据 code 获取 openid
		$openid = DataUserWxappModel::getWxUserOpenid($code);

		$wxuserinfo['openid'] = $openid;
		try {
			validate(DataUserValidate::class)->check($post_data);
		} catch (ValidateException $e) {
			abort(200, $e->getMessage());
		}
		$user = new DataUserModel;
		$user->save($post_data);
		$user->wxapp()->save($wxuserinfo);
		if ($user) {
			return success(200, "创建成功~", $user);
		} else {
			return error(101, "注册失败~");
		}
	}
	**/

}
