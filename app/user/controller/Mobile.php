<?php

namespace app\user\controller;

use app\common\model\DataUser as DataUserModel;
use app\common\validate\DataUser as DataUserValidate;
use think\exception\ValidateException;
use think\facade\Request;

class Mobile
{
	public function index()
	{
		return success(0, "mobile-index");
	}

	public function bind()
	{
		$m = Request::only(['id', 'mobile']);
		try {
			validate(DataUserValidate::class)->scene('bind')->check([
				'mobile' => $m['mobile']
			]);
		} catch (ValidateException $e) {
			abort(1001, $e->getMessage());
		}

		$u = DataUserModel::update($m);
		if ($u) {
			$r_user = DataUserModel::withJoin('wxapp')->find($u->id);
			return success(200, "操作成功~", $r_user);
		}

	}

	public function login()
	{
		// 实例化模型对象
		$user = new \app\common\model\DataUser;
		dump($user);
		return success(0, "mobile-login");
	}

	public function reg()
	{
		$data = Request::only(['mobile', 'realname']);
		try {
			validate(DataUserValidate::class)->check($data);
		} catch (ValidateException $e) {
			abort(200, $e->getMessage());
		}
		// 实例化模型
		$user = new DataUserModel;
		$user->save($data);
		if ($user->id) {
			return success(200, "注册成功~", $user);
		} else {
			return error(201, "注册失败~");
		}

	}
}



/*
 * 

region_city
region_province
region_area
base_age
base_sex
base_height
base_weight
base_birthday
ext_company
ext_section
wx_openid
wx_avatar
score_total
score_used






 */