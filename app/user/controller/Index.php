<?php

namespace app\user\controller;

use think\facade\View;

class Index
{
	public function index ()
	{
		return View::fetch('index');
	}
}