<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;


Route::get('index$', '\app\user\controller\Index@index');

// 用户
Route::post('wxapp/login', '\app\user\controller\Wxapp@login');
Route::post('wxapp/reg', '\app\user\controller\Wxapp@reg');
Route::post('wxapp/speedy/login', '\app\user\controller\Wxapp@speedyLogin');
Route::post('wxapp/edit', '\app\user\controller\Wxapp@edit');

Route::post('login/index', '\app\user\controller\Login@index');
Route::post('login/auth', '\app\user\controller\Login@auth');

Route::group(function(){
	Route::post('wxapp/complete', '\app\user\controller\Wxapp@complete');
})->middleware('check_token');

// 用户手机操作
Route::post('mobile/bind', '\app\user\controller\Mobile@bind');
Route::post('mobile/login', '\app\user\controller\Mobile@login');
Route::post('mobile/reg', '\app\user\controller\Mobile@reg');

Route::get('welcome', function () {
	return "hello, welcome";
});


// Route::get('new/<id>','News/read'); // 定义GET请求路由规则
// Route::post('new/<id>','News/update'); // 定义POST请求路由规则
// Route::put('new/:id','News/update'); // 定义PUT请求路由规则
// Route::delete('new/:id','News/delete'); // 定义DELETE请求路由规则
// Route::any('new/:id','News/read'); // 所有请求都支持的路由规则



// 当路由规则都不匹配的话，就会跳到`miss`
Route::miss(function () {
    return json(['code'=>10001, 'msg'=>"异常访问"]);
});