<?php
namespace app;

use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\exception\Handle;
use think\exception\HttpException;
use think\exception\HttpResponseException;
use think\exception\ValidateException;
use think\Response;
use Throwable;

use app\common\exception\BaseException;

/**
 * 应用异常处理类
 */
class ExceptionHandle extends Handle
{
    public $httpStatus = 500;
    private $code;
    private $msg;
    private $errorCode;

    /**
     * 不需要记录信息（日志）的异常类列表
     * @var array
     */
    protected $ignoreReport = [
        HttpException::class,
        HttpResponseException::class,
        ModelNotFoundException::class,
        DataNotFoundException::class,
        ValidateException::class,
    ];

    /**
     * 记录异常信息（包括日志或者其它方式记录）
     *
     * @access public
     * @param  Throwable $exception
     * @return void
     */
    public function report(Throwable $exception): void
    {
        // 使用内置的方式记录异常日志
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @access public
     * @param \think\Request   $request
     * @param Throwable $e
     * @return Response
     */
    public function render($request, Throwable $e): Response
    {
        // // 添加自定义异常处理机制
        // if ($e instanceof BaseException) {
        //     $this->code = $e->code;
        //     $this->msg = $e->msg;
        //     $this->errorCode = $e->errorCode;
        // } else {
        //     if (env('APP_DEBUG', '0')) {
        //         // 调试模式下，其他错误交给系统处理
        //         return parent::render($request, $e);
        //     } else {
        //         $this->code = 500;
        //         $this->msg = "服务器错误";
        //         $this->errorCode = 999;
        //     }
        // }

        // $result = [
        //     'error_code' => $this->errorCode,
        //     'msg' => $this->msg,
        //     'request_url' => request()->url()
        // ];
        
        // return json($result, $this->code);

        // 参数验证
        if ($e instanceof ValidateException) {
            return json([
                'code' => 422,
                'msg' => $e->getError(),
            ], 200);
        }

        // 调试模式
        if (env('APP_DEBUG')) {
            // 调试模式下，其他错误交给系统处理
            return parent::render($request, $e);
        }

        $headers = $e->getHeaders();

        return json([
            'msg' => $e->getMessage(),
            'code' => $e->getStatusCode()
        ], array_key_exists('statusCode', $headers) ? $headers['statusCode'] : 200);
 
    }
}
