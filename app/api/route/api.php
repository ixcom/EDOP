<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

// 用户
Route::get('user/index', '\app\api\controller\User@index');
Route::post('user/demo', '\app\api\controller\User@demo')->middleware(\app\api\middleware\CheckToken::class);
// Route::post('user/demo', '\app\api\controller\User@demo');
Route::post('user/reg', '\app\api\controller\User@reg');

// Route::get('new/<id>','News/read'); // 定义GET请求路由规则
// Route::post('new/<id>','News/update'); // 定义POST请求路由规则
// Route::put('new/:id','News/update'); // 定义PUT请求路由规则
// Route::delete('new/:id','News/delete'); // 定义DELETE请求路由规则
// Route::any('new/:id','News/read'); // 所有请求都支持的路由规则

// 当路由规则都不匹配的话，就会跳到`miss`
Route::miss(function () {
    return json(['code'=>10001, 'msg'=>"异常访问"]);
});