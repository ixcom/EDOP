<?php
namespace app\api\service;

use Firebase\JWT\JWT;
use think\facade\Config;

class Token
{
	public static function create ($admin)
	{
		$jwt = Config::get('jwt');
		$key = config('jwt.key');
		$jwt['data'] = $admin;
		return JWT::encode($jwt, $key);
	}


}