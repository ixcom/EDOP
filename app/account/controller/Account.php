<?php
namespace app\account\controller;

use app\BaseController;
use think\App;

class Account extends BaseController
{

	/**
     * 构造方法
     * @access public
     * @param  App  $app  应用对象
     */
    public function __construct(App $app)
    {
        // 控制器初始化
        $this->initialize();
    }


    // 初始化
    protected function initialize()
    {
    }
}