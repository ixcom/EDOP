<?php
namespace app\account\controller;

use think\facade\View;

class Index extends Account
{
	public function index ()
	{
		return View::fetch("index/index");
	}
}