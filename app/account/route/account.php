<?php

use think\facade\Route;

Route::get('index$', '\app\account\controller\Index@index');


// 当路由规则都不匹配的话，就会跳到`miss`
Route::miss(function () {
    return json(['code'=>10001, 'msg'=>"异常访问"]);
});