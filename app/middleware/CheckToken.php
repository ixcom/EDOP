<?php
declare (strict_types = 1);

namespace app\middleware;
use app\common\lib\Token;

class CheckToken
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        $token = request()->header('token');

        $res = Token::checkJwt($token);
        
        if ($res['code'] != 1) {
            return json($res);
        }

        $request->id = $res['data'];

        return $next($request);
    }
}
